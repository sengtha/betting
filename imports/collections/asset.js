import SimpleSchema from 'simpl-schema';

const Schemas = {};
export const Asset = new Meteor.Collection('Asset');
Schemas.Asset = new SimpleSchema({
        name: {
                type: String,
                max:100
        },
        symbol: {
                type: String
        },
        createdAt: {
                type: Date,
                defaultValue: new Date()
        }
});

Asset.attachSchema(Schemas.Asset);

//Add user role
if(Meteor.isServer){
        
        Asset._ensureIndex(
                {symbol:1 }, { unique: true }
        );
}
