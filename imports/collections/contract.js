import SimpleSchema from 'simpl-schema';

const Schemas = {};
export const Contract = new Meteor.Collection('Contract');
Schemas.Contract = new SimpleSchema({
        name: {
                type: String,
                max:100
        },
        address: {
                type: String
        },
        available: {
                type: Boolean,
                defaultValue: true
        },
        createdAt: {
                type: Date,
                defaultValue: new Date()
        }
});

Contract.attachSchema(Schemas.Contract);

//Add user role
if(Meteor.isServer){
        
        Contract._ensureIndex(
                {address:1 }, { unique: true }
        );
}
