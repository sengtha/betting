import SimpleSchema from 'simpl-schema';
import { Contract } from './contract.js';
import { Asset } from './asset.js';

const Schemas = {};
export const Betting = new Meteor.Collection('Betting');
Schemas.Betting = new SimpleSchema({
        contract: {
                type: Contract,
                regEx: SimpleSchema.RegEx.Id
        },
        asset: {
                type: Asset,
                regEx: SimpleSchema.RegEx.Id
        },
        start_price: {
                type: Number,
                defaultValue: 0
        },
        end_price: {
                type: Number,
                defaultValue: 0
        },
        cancel_block: {
                type: Number,
                defaultValue: 0
        },
        start_block: {
                type: Number,
                defaultValue: 0
        },
        end_block: {
                type: Number,
                defaultValue: 0
        },
        expired_block: {
                type: Number,
                defaultValue: 0
        },
        min_up: {
                type: Number,
                defaultValue: 1
        },
        min_down: {
                type: Number,
                defaultValue: 1
        },
        bet_up: {
                type: Number,
                defaultValue: 0
        },
        bet_down: {
                type: Number,
                defaultValue: 0
        },
        total_up: {
                type: Number,
                defaultValue: 0
        },
        total_down: {
                type: Number,
                defaultValue: 0
        },
        intent_up: {
                type: Number,
                defaultValue: 0
        },
        intent_down: {
                type: Number,
                defaultValue: 0
        },
        status: {
                type: Number,
                defaultValue: 0 // 0 = Accept intent, 1 = Submitting, 2 = Started, 3= blocked, 4 = End, 5 = Expired
        },
        claim_amount: {
                type: Number,
                defaultValue: 0
        },
        claim_txid: {
                type: String,
                optional: true
        },
        created_at_start: {
                type: Date,
                defaultValue: new Date()
        },
        created_at_end: {
                type: Date,
                optional: true
        }
});

Betting.attachSchema(Schemas.Betting);


if(Meteor.isServer){
        Betting.before.insert(function (userId, doc) {
        });
        Betting.after.insert(function (userId, doc) {
                
        });
        Betting.after.update(function (userId, doc, fieldNames, modifier, options) {
                //Smart contract has been reset
                if(doc.status === 5){
                        Contract.update({_id: doc.contract}, {$set:{available: true}});
                }else{
                        Contract.update({_id: doc.contract}, {$set:{available: false}});
                }
        });

        Betting._ensureIndex(
                {contract: 1}
        );
        Betting._ensureIndex(
                {contract: 1, asset: 1}
        );
}
