import SimpleSchema from 'simpl-schema';
import { Betting } from './betting.js';

const Schemas = {};
export const BettingAddress = new Meteor.Collection('BettingAddress');
Schemas.BettingAddress = new SimpleSchema({
        betting: {
                type: Betting,
                regEx: SimpleSchema.RegEx.Id
        },
        address: {
                type: String
        },
        amount: {
                type: Number,
                defaultValue: 0
        },
        reward: {
                type: Number,
                defaultValue: 0
        },
        bet_txid: {
                type: String,
                optional: true
        },
        claim_txid: {
                type: String,
                optional: true
        },
        withdraw: {
                type: Number,
                defaultValue: 0
        },
        up: {
                type: Boolean,
                defaultValue: false
        },
        down: {
                type: Boolean,
                defaultValue: false
        },
        submitted: {
                type: Boolean,
                defaultValue: false
        },
        won: {
                type: Boolean,
                defaultValue: false
        },
        claim: {
                type: Boolean,
                defaultValue: false
        },
        draw: {
                type: Boolean,
                defaultValue: false
        },
        created_at: {
                type: Date,
                defaultValue: new Date()
        },
        submitted_at: {
                type: Date,
                optional: true
        },
        claimed_at: {
                type: Date,
                optional: true
        }
});

BettingAddress.attachSchema(Schemas.BettingAddress);


if(Meteor.isServer){
        BettingAddress.before.insert(function (userId, doc) {
        });
        BettingAddress.after.insert(function (userId, doc) {
                if(doc.up === true) Betting.update(doc.betting, {$inc:{intent_up:1}});
                if(doc.down === true) Betting.update(doc.betting, {$inc:{intent_down:1}});
        });
        BettingAddress.after.update(function (userId, doc, fieldNames, modifier, options) {
        });

        BettingAddress._ensureIndex(
                {betting: 1}
        );
        BettingAddress._ensureIndex(
                {betting: 1, address:1 }, { unique: true }
        );
}
