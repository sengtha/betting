import { Tabular } from 'meteor/aldeed:tabular';
import { Asset } from '../collections/asset.js';
import { Contract } from '../collections/contract.js';

export const TabularTables = {};

TabularTables.Asset = new Tabular.Table({
        name: "Asset",
        collection: Asset,
        responsive: true,
        bFilter: true,
        stateSave: true,
        autoWidth: false,
        columns: [

                {data: 'name', title: 'Title' },
                {data: 'symbol', title: 'Symbol' },
                {
                        data: "_id", width:'200px', orderable: false, className: "text-center",
                        title: "Action",
                        render: function (e){
                                var btntxt = new Spacebars.SafeString("<a href='' id='edit_asset' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Change</a> ");
                                return btntxt + new Spacebars.SafeString("<a href='' id='delete_asset' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>");
                        }
                }
        ]
});

TabularTables.Contract = new Tabular.Table({
        name: "Contract",
        collection: Contract,
        responsive: true,
        bFilter: false,
        stateSave: true,
        autoWidth: false,
        columns: [

                {data: 'name', title: 'Name' },
                {data: 'address', title: 'Address' },
                {data: 'available', title: 'Available' },
                {
                        data: "_id", width:'200px', orderable: false, className: "text-center",
                        title: "Action",
                        render: function (e){
                                var btntxt = new Spacebars.SafeString("<a href='' id='edit_contract' class='btn btn-info btn-xs'><i class='fa fa-edit'></i> Change</a> ");
                                return btntxt + new Spacebars.SafeString("<a href='' id='delete_contract' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>");
                        }
                }
        ]
});
