import { Asset } from '../imports/collections/asset.js';
import { Contract } from '../imports/collections/contract.js';
import { Betting } from '../imports/collections/betting.js';
import { BettingAddress } from '../imports/collections/betting_address.js';

/* Zilliqa API */
const { BN, Long, bytes, units } = require('@zilliqa-js/util');
const { Zilliqa } = require('@zilliqa-js/zilliqa');
const CP = require ('@zilliqa-js/crypto');
/* CoinGecko API */
const CoinGecko = require('coingecko-api');
const CoinGeckoClient = new CoinGecko();

/** Zilliqa Testnet */
const zilliqa = new Zilliqa(Meteor.settings.public.zilliqa.testnet.host);
const CHAIN_ID = Meteor.settings.public.zilliqa.testnet.chainid;
const MSG_VERSION = Meteor.settings.public.zilliqa.testnet.msgversion;
/* Zilliq Mainnet */
// const zilliqa = new Zilliqa(Meteor.settings.public.zilliqa.mainnet.host);
// const CHAIN_ID = Meteor.settings.public.zilliqa.testnet.mainnet.chainid;
// const MSG_VERSION = Meteor.settings.public.zilliqa.testnet.mainnet.msgversion;

const VERSION = bytes.pack(CHAIN_ID, MSG_VERSION);
 

SyncedCron.add({
    name: 'Adding new bet',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        addBetting();
    }
});
SyncedCron.add({
    name: 'Cancel bet',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        cancelBetting(); 
    }
});
SyncedCron.add({
    name: 'Submit to sc',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        startBetting(); 
    }
});

SyncedCron.add({
    name: 'Stop betting',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        blockBetting();
    }
});

SyncedCron.add({
    name: 'End betting',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        endBetting();
    }
});

SyncedCron.add({
    name: 'Expire betting',
    schedule: function(parser) {
            return parser.text('every 1 mins');
    },
    job: function() {
        expiredBetting();
    }
});


async function addBetting(){
    let start_price = 0;  
    let contract = Contract.findOne({available: true});
    let asset = Asset.aggregate([ {$match: {}}, {$sample:{size:1}}]);
    if(contract && asset && asset.length>0){

        const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
        let current_block = parseInt(numTxBlock.result);
        // About 15 minutes from current block
        let cancel_block = current_block + 20; 
        // About 30 minutes from cancel block
        let start_block = current_block + 40;
        // About 60 minutes from start block
        let end_block = start_block+80;
        // About 30 minites from end block 
        let expired_block = end_block + 40;
        //console.log(expired_block);
        
        let betting = Betting.find({$or:[{status: 0}, {status: 1}, {status: 2}]}).count();
        if(betting === 0){
            /* Get asset price from CoinGecko API */
            let geckodata = await CoinGeckoClient.coins.fetch(asset[0].symbol, {});
            if(geckodata) start_price = geckodata.data.market_data.current_price.usd;
            /* If there is price */  
            if(start_price !== 0){
                Betting.insert({
                    contract: contract._id, asset: asset[0]._id, start_price: start_price, 
                    cancel_block: cancel_block, start_block: start_block, end_block: end_block, expired_block: expired_block,
                    min_up:1, min_down: 1
                })
            }
        }
    }
}
async function blockBetting(){
    Betting.find({status: 2}).forEach(async betting => {
        if(betting){
            const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
            let current_block = parseInt(numTxBlock.result);
            if((current_block > betting.start_block))
            {
                Betting.update({_id:betting._id}, {$set:{status: 3}});
            }
        
        }
    });
}

async function startBetting(){
    
    Betting.find({status: 0}).forEach(async betting =>{
        if(betting){
            const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
            let current_block = parseInt(numTxBlock.result);
            if((current_block <= betting.cancel_block) && betting.intent_up >= betting.min_up && betting.intent_down >= betting.min_down)
            {
                
                Betting.update({_id:betting._id}, {$set:{status: 1}});
                let dbcontract = Contract.findOne({_id: betting.contract});
                let key = Meteor.settings.owner.secret;
                console.log(key);
                try{
                    zilliqa.wallet.addByPrivateKey(key);
                    const myGasPrice = await zilliqa.blockchain.getMinimumGasPrice();
                    const smartContract = await zilliqa.contracts.at(dbcontract.address);

                    let sprice = parseInt(betting.start_price*100);
                    if(betting.start_price<1) sprice = accounting.formatMoney(betting.start_price, "", 4, ",", ".")*10000;
                    const callTx = await smartContract.call(
                        "Start",
                        [
                            {
                                vname: "price",
                                type: "Uint128",
                                value: `${sprice}`
                            },
                            {
                                vname: "betblock",
                                type: "BNum",
                                value: `${betting.start_block}`
                            },
                            {
                                vname: "endblock",
                                type: "BNum",
                                value: `${betting.end_block}`
                            },
                            {
                                vname: "expiredblock",
                                type: "BNum",
                                value: `${betting.expired_block}`
                            },
                            {
                                vname: "fee",
                                type: "Uint128",
                                value: "3"
                            }
                        ],
                        {
                            version: VERSION,
                            amount: new BN(0),
                            gasPrice: new BN(myGasPrice.result),
                            gasLimit: Long.fromNumber(Meteor.settings.public.zilliqa.gas),
                        }
                    );
                    console.log(callTx);
                    if(callTx && callTx.receipt && callTx.receipt.event_logs && callTx.receipt.event_logs.length>0){
                        console.log(callTx.receipt);
                        let rstext = callTx.receipt.event_logs[0]._eventname;
                        if(rstext === "Started") {
                            Betting.update({_id:betting._id}, {$set:{status: 2}});
                        }
                    }
                }catch(error){
                    console.log(error);
                }
            }
        
        }
    });
    
}

async function endBetting(){
    let end_price = 0;
    //3= blocked
    Betting.find({status: 3}).forEach(async betting => {
        if(betting){
            const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
            let current_block = parseInt(numTxBlock.result);
            if((current_block >= betting.end_block))
            {

                let key = Meteor.settings.owner.secret;
                /* Get asset price from CoinGecko API */
                let asset = Asset.findOne({_id: betting.asset});
                let geckodata = await CoinGeckoClient.coins.fetch(asset.symbol, {});
                if(geckodata) end_price = geckodata.data.market_data.current_price.usd;
                if(end_price !== 0)
                {
                    var origin_end_price = end_price;
                    Betting.update({_id:betting._id}, {$set:{status: 33, end_price: end_price, created_at_end: new Date()}});
                    try{
                        zilliqa.wallet.addByPrivateKey(key);
                        let dbcontract = Contract.findOne({_id: betting.contract});
                        const myGasPrice = await zilliqa.blockchain.getMinimumGasPrice();//units.toQa('1000', units.Units.Li);
                        const smartContract = await zilliqa.contracts.at(dbcontract.address);
                        
                        if(end_price<1) {
                            end_price = accounting.formatMoney(end_price, "", 4, ",", ".")*10000;
                        }else{
                            end_price = end_price*100;
                        }
                        const callTx = await smartContract.call(
                            "End",
                            [
                                {
                                    vname: "price",
                                    type: "Uint128",
                                    value: `${end_price}`
                                }
                            ],
                            {
                                version: VERSION,
                                amount: new BN(0),
                                gasPrice: new BN(myGasPrice.result),
                                gasLimit: Long.fromNumber(Meteor.settings.public.zilliqa.gas),
                            }
                        );
                        console.log(callTx);
                        if(callTx && callTx.receipt && callTx.receipt.event_logs && callTx.receipt.event_logs.length>0){
                            console.log(callTx.receipt);
                            let rstext = callTx.receipt.event_logs[0]._eventname;
                            if(rstext === "Ended") {

                                //Get state
                                const smartContracts = await zilliqa.blockchain.getSmartContractState(
                                    CP.fromBech32Address(dbcontract.address).toLowerCase()
                                );
                                total_up = units.fromQa(new BN(smartContracts.result.total_up), units.Units.Zil);
                                total_down = units.fromQa(new BN(smartContracts.result.total_down), units.Units.Zil);

                                Betting.update({_id:betting._id}, {$set:{status: 4, total_up: total_up, total_down: total_down}});
                                if(parseFloat(betting.start_price) > parseFloat(origin_end_price))
                                {
                                    BettingAddress.update({betting: betting._id, down: true}, {$set:{won: true}}, {multi:true});
                                }else if (parseFloat(betting.start_price) === parseFloat(origin_end_price)) {
                                    BettingAddress.update({betting: betting._id}, {$set:{draw: true}}, {multi:true})
                                }else{
                                    BettingAddress.update({betting: betting._id, up: true}, {$set:{won: true}}, {multi:true})
                                }
                            }else{
                                //Betting.update({_id:betting._id}, {$set:{status: 3}});
                            }
                        }
                    }catch(error){
                        console.log(error);
                        Betting.update({_id:betting._id}, {$set:{status: 3}});
                    }
                }
            }
        
        }
    });
    
}

async function expiredBetting(){
    
    Betting.find({status: 4}).forEach(async betting => {
        if(betting){
            const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
            let current_block = parseInt(numTxBlock.result);
            if((current_block >= betting.expired_block))
            {
                let key = Meteor.settings.owner.secret;
                Betting.update({_id:betting._id}, {$set:{status: 44}});
                try{
                    zilliqa.wallet.addByPrivateKey(key);
                    let dbcontract = Contract.findOne({_id: betting.contract});
                    const myGasPrice = await zilliqa.blockchain.getMinimumGasPrice();//units.toQa('1000', units.Units.Li);
                    const smartContract = await zilliqa.contracts.at(dbcontract.address);
                    const callTx = await smartContract.call(
                        "ClaimCommission",
                        [],
                        {
                            version: VERSION,
                            amount: new BN(0),
                            gasPrice: new BN(myGasPrice.result),
                            gasLimit: Long.fromNumber(Meteor.settings.public.zilliqa.gas),
                        }
                    );
                    console.log(callTx);
                    if(callTx && callTx.receipt && callTx.receipt.event_logs && callTx.receipt.event_logs.length>0){
                        console.log(callTx.receipt);
                        let rstext = callTx.receipt.event_logs[0]._eventname;
                        if(rstext === "ClaimedCommission") {
                            let reward = units.fromQa(new BN(callTx.receipt.event_logs[0].params[1].value), units.Units.Zil);
                            Betting.update({_id:betting._id}, {$set:{status: 5, claim_txid: callTx.id, claim_amount: reward}});
                        }
                    }
                }catch(error){
                    console.log(error);
                    Betting.update({_id:betting._id}, {$set:{status: 4}});
                }
                
            }
        
        }
    });
    
}
async function cancelBetting(){
    let start_price = 0;
    let betting = Betting.findOne({status: 0});
    if(betting){
        const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
        let current_block = parseInt(numTxBlock.result);
        if((current_block > betting.cancel_block) && betting.status === 0)
        {
            // About 15 minutes from current block
            let cancel_block = current_block + 20; 
            // About 30 minutes from cancel block
            let start_block = current_block + 40;
            // About 60 minutes from start block
            let end_block = start_block+80;
            // About 30 minites from end block
            let expired_block = end_block + 40;

            /* Get asset price from CoinGecko API */
            let asset = Asset.findOne({_id: betting.asset});
            if(asset){
                let geckodata = await CoinGeckoClient.coins.fetch(asset.symbol, {});
                if(geckodata) start_price = geckodata.data.market_data.current_price.usd;
                    /* If there is price */  
                    if(start_price !== 0){
                        BettingAddress.remove({betting: betting._id},{multi:true});
                        Betting.update({_id: betting._id},{$set:{ intent_up: 0, intent_down:0, 
                            start_price: start_price, cancel_block: cancel_block, start_block: start_block, end_block: end_block, expired_block: expired_block}
                        })
                    }
                }
            }
            
    }
}



Meteor.startup(function() {
    SyncedCron.start();
})


//Not successfully bet
//0x5026d0dd67a896c48796448114c3bc193b1e0ea6ccb5c53f5e91e089339f14a9
//0xba43fede9203826a0f287bbf50cab1a071ed5cdcb57e1e04b41eca5850c37187