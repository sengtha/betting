import { Meteor } from 'meteor/meteor';
import { Roles } from 'meteor/alanning:roles';
import { Asset } from "../imports/collections/asset.js";
import { Contract } from "../imports/collections/contract.js";
import "../imports/common/tabular.js";
import "./cron.js";
import { Betting } from '../imports/collections/betting.js';
import { BettingAddress } from '../imports/collections/betting_address.js';

//For publishing role
Meteor.publish(null, function () {
  if (this.userId) {
    return Meteor.roleAssignment.find({ 'user._id': this.userId });
  } else {
    this.ready()
  }
})


Meteor.publish('availableBetting', function(){
	return Betting.find({$or:[{status: 0}, {status: 1}, {status: 2}]});
});

Meteor.publish('ongoingBetting', function(address){
  let bettings = Betting.find({$or:[{status: 3}, {status: 33},{status: 4}, {status: 44}]}).map(function(element){return element._id;});
  return BettingAddress.find({betting:{$in: bettings}, address: address});
});

Meteor.publish('completedBetting', function(address){
  let bettings = Betting.find({status: 5}).map(function(element){return element._id;});
  return BettingAddress.find({betting:{$in: bettings}, address: address});
});

Meteor.publish('bettingById', function(id){
  return Betting.find({_id: id});
});
Meteor.publish('assetById', function(id){
  return Asset.find({_id: id});
});

Meteor.publish('bettingAddress', function(betting, address){
	return BettingAddress.find({betting: betting, address: address});
});
Meteor.publish('contractById', function(id){
	return Contract.find({_id: id});
});
//Method
Meteor.methods({
  getBettingAddressById: function(id) {
    let baddr = BettingAddress.findOne({_id:id});
    if(baddr) {
       let beeting = Betting.findOne({_id:baddr.betting});
       if(beeting) return {bettingid: beeting._id, contractid: beeting.contract, up: baddr.up, down: baddr.down};
    }
    return false;
  },
  updateClaiming: function (id, address, txid) {
    BettingAddress.update({betting:id, address: address}, {$set:{ claim_txid: txid}});
  },
  updateClaimed: function (txid, reward) {
    BettingAddress.update({claim_txid:txid}, {$set:{ claim: true, reward: reward, claimed_at: new Date()}});
  },
  updateUpDown: function (id, up, down) {
    Betting.update({_id:id}, {$set:{ bet_up: up, bet_down: down }});
  },
  updateSubmitting: function (id, address, amount, txid, status) {
    let exist = BettingAddress.find({betting:id, address: address}).count();
    if(exist>0)
    {
      BettingAddress.update({betting:id, address: address}, {$set:{ amount: amount, bet_txid: txid }});
    }else{
      if(status === "up") BettingAddress.insert({betting:id, address: address, up: true, amount: amount, bet_txid: txid});
      if(status === "down") BettingAddress.insert({betting:id, address: address, down: true, amount: amount, bet_txid: txid});
    }
    
  },
  updateSubmitted: function (id, address) {
    BettingAddress.update({betting:id, address: address}, {$set:{ submitted: true, submitted_at: new Date()}});
  },
  addAsset: function (name, symbol) {
    return Asset.insert({ name: name, symbol: symbol });
  },
  updateAsset: function (id, name, symbol) {
    return Asset.update({_id: id}, {$set:{ name: name, symbol: symbol }});
  },
  getAsset: function (id) {
    return Asset.findOne({ _id: id });
  },
  deleteAsset: function (id) {
    return Asset.remove({ _id: id });
  },
  addContract: function (name, address) {
    return Contract.insert({ name: name, address: address });
  },
  updateContract: function (id, name, address) {
    return Contract.update({_id: id}, {$set:{ name: name, address: address }});
  },
  getContract: function (id) {
    return Contract.findOne({ _id: id });
  },
  deleteContract: function (id) {
    return Contract.remove({ _id: id });
  },
  intentUp: function (betting, address) {
    return BettingAddress.insert({betting:betting, address: address, up: true});
  },
  intentDown: function (betting, address) {
    return BettingAddress.insert({betting:betting, address: address, down: true});
  }
});

//On Startup
Meteor.startup(() => {
  var myroles = ['admin'];
  // this will fail if the roles package isn't installed
  if (Meteor.roles.find().count() === 0) {
    myroles.map(function (role) {
      Roles.createRole(role)
    })
  };
});

var user = Accounts.findUserByEmail("sengtha@gmail.com");
var rs = Roles.setUserRoles(user._id, ['admin']); 
//console.log(Meteor.roleAssignment.find({}).fetch());
