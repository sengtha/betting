
import {Router} from "meteor/iron:router";

Router.configure({
    notFoundTemplate: "notFound"
})

Router.plugin('ensureSignedIn', {
    except: ['public','ongoing', 'completed', 'atSignIn', 'atSignUp']
});

Router.route('/', {name: 'public'});
Router.route('/logout', function () {
        Meteor.logout(function (err){
                if(!err) Router.go('/');
        });

});
Router.route('/ongoing', function () {
        this.render('ongoing');
});
Router.route('/completed', function () {
        this.render('completed');
});
Router.route('/asset', function () {
        if (!Roles.userIsInRole(Meteor.user(), ['admin'])) Router.go('/');
        this.render('asset');
});
Router.route('/contract', function () {
        if (!Roles.userIsInRole(Meteor.user(), ['admin'])) Router.go('/');
        this.render('contract');
});
