import { Template } from 'meteor/templating';
import { BettingAddress } from '../imports/collections/betting_address.js';
import { Betting } from '../imports/collections/betting.js';
import { Asset } from '../imports/collections/asset.js';

const { Zilliqa } = require('@zilliqa-js/zilliqa');
const { BN, units } = require('@zilliqa-js/util');
const zilliqa = new Zilliqa(Meteor.settings.public.zilliqa.testnet.host);



Template.completed.helpers({
    zilpay: function (event){
		if (Session.get("wallet") !== 'undefined') 
		{
			return Session.get("wallet");
		}
		return false;
	},
	asset: function(id){
		Meteor.subscribe("assetById", id);
		let asset = Asset.findOne({_id:id});
		if(asset) return asset.name;
	},
	bettingadd: function(event){
			Meteor.subscribe("completedBetting", Session.get("wallet16"));
		return BettingAddress.find();
	},
	betting: function(id){
			Meteor.subscribe("bettingById", id);
			return Betting.findOne({_id:id});
	}
        
})

Template.completed.onRendered(function (event) {
    
	Meteor.setTimeout(function(){
		
		if (typeof window.zilPay !== 'undefined') 
		{
			
			powerBalance();
			Meteor.setInterval(function(){
				powerBalance();
			}, 22500);
			//console.log(window.zilPay.wallet);
			window.zilPay.wallet.observableAccount().subscribe(function (account) {
				checkZilpayAccount(account);
			});
			window.zilPay.wallet.observableNetwork().subscribe(function (net) {
				checkZilPayNetwork(net);
			});
		}
		//console.log(window.zilPay.wallet.defaultAccount);
	}, 1200);
});

Template.completed.events({                
    
});


function checkZilpayAccount(account){
	console.log("Account changed:");
	if(account && account.bech32 !==""){
		Session.set("wallet", account.bech32);
		Session.set("wallet16", account.base16);
		localStorage.setItem("bech32", account.bech32);
		localStorage.setItem("base16", account.base16);
		$("#wallet_balance").text("");
	}
}
function checkZilPayNetwork(net){
	console.log("Network changed:");
	console.log(net);
}

//Get power balance for any address
async function powerBalance(){
	const balance = await zilliqa.blockchain.getBalance(localStorage.getItem("base16"));
	//console.log(balance);
	let result = 0;
	if(balance && balance.result && balance.result.balance)
	{
			result = units.fromQa(new BN(balance.result.balance), units.Units.Zil);
	}
	Session.set("wallet_balance", result);
	$("#wallet_balance").text(result);
}