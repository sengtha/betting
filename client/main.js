import { Template } from 'meteor/templating';
import { Contract } from '../imports/collections/contract.js';
import { Betting } from '../imports/collections/betting.js';
import { BettingAddress } from '../imports/collections/betting_address.js';
import { Asset } from '../imports/collections/asset.js';

import './user.js';
import './router.js';
import './main.html';
import '../imports/common/tabular.js';
import './asset.html';
import './asset.js';
import './contract.html';
import './contract.js';
import './ongoing.html';
import './ongoing.js';
import './completed.html';
import './completed.js';

const { Zilliqa } = require('@zilliqa-js/zilliqa');
const { BN, units } = require('@zilliqa-js/util');
const CP = require ('@zilliqa-js/crypto');
const zilliqa = new Zilliqa(Meteor.settings.public.zilliqa.testnet.host);

var timerID = null;
var txID = "";

UI.registerHelper('formatcurrency', function(number){
	if(number<1) return accounting.formatMoney(number, "", 4, ",", ".");
	return accounting.formatMoney(number);
});
UI.registerHelper('formatdatetime', function(e){
	return moment(e).format("MM/DD/YYYY HH:mm:ss");
});

Meteor.subscribe("availableBetting");

Template.public.helpers({
    zilpay: function (event){
		if (Session.get("wallet") !== 'undefined') 
		{
			return Session.get("wallet");
		}
		return false;
	},
	betting: function(event){
		return Betting.findOne();
	},
	asset: function(id){
		Meteor.subscribe("assetById", id);
		let asset = Asset.findOne({_id:id});
		if(asset) return asset.name;
	},
	address: function(event){
		let betting = Betting.findOne();
		Meteor.subscribe("bettingAddress", betting._id, Session.get("wallet16"));
		return BettingAddress.findOne();
	}
    
})
Template.public.onRendered(function (event) {
	
	updateBlock();
	Meteor.setInterval(function(){
		updateBlock();
	}, 22500);
	Meteor.setTimeout(function(){
		if (typeof window.zilPay !== 'undefined') 
		{
			
			powerBalance();
			Meteor.setInterval(function(){
				powerBalance();
			}, 22500);
			try{
				//console.log(window.zilPay.wallet);
				window.zilPay.wallet.observableAccount().subscribe(function (account) {
					checkZilpayAccount(account);
				});
				window.zilPay.wallet.observableNetwork().subscribe(function (net) {
					checkZilPayNetwork(net);
				});
			}catch(e){
				console.log(e);
			}
			
		}
		//console.log(window.zilPay.wallet.defaultAccount);
	}, 1200);


});


Template.public.events({
	'click #betting_up': function(event){
		let amount = $("#zilamount").val();
		if(amount && parseInt(amount) >=10){
			betUp(amount);
		}else{
			alert("Please input at least 10 zil.");
		}
		
	},
	'click #betting_down': function(event){
		let amount = $("#zilamount").val();
		if(amount && parseInt(amount) >=10){
			betDown(amount);
		}else{
			alert("Please input at least 10 zil.");
		}
		
	},
	'click #intent_up': function(event){
		let betting = Betting.findOne();
		if(betting) Meteor.call("intentUp", betting._id, localStorage.getItem("base16"));
	},
	'click #intent_down': function(event){
		let betting = Betting.findOne();
		if(betting) Meteor.call("intentDown", betting._id, localStorage.getItem("base16"));
	},
    'click #accessZilpay': function(event){
		window.zilPay.wallet.connect();
    }
});

function checkZilpayAccount(account){
	console.log("Account changed:");
	if(account && account.bech32 !==""){
		Session.set("wallet", account.bech32);
		Session.set("wallet16", account.base16);
		localStorage.setItem("bech32", account.bech32);
		localStorage.setItem("base16", account.base16);
		$("#wallet_balance").text("");
	}
}
function checkZilPayNetwork(net){
	console.log("Network changed:");
	console.log(net);
}
async function updateBlock(){
	
		const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
		$("#current_block").html(numTxBlock.result);
		$(".currentblock").attr("value",numTxBlock.result);
}

async function betUp(zilamount){
	const isConnect = await window.zilPay.wallet.connect();
	
	if (isConnect) {
		$("#overlay").show();
		let betting = Betting.findOne();
		
		Meteor.subscribe("contractById", betting.contract, async function(){
			let gcontract = Contract.findOne();
			const { contracts, utils, wallet } = window.zilPay;
			const contract = contracts.at(gcontract.address);
			const amount = utils.units.toQa(zilamount, utils.units.Units.Zil);
			const gasPrice = utils.units.toQa('1000', utils.units.Units.Li);
			try{
				const tx = await contract.call(
					'Up',[],
					{
						amount,
						gasPrice,
						gasLimit: utils.Long.fromNumber(2000)
					},
					true
				).then((tx) => {
					console.log(tx);
					txID = tx.TranID;
					Meteor.call("updateSubmitting", betting._id, localStorage.getItem("base16"), zilamount, txID, "up");
					timerID = Meteor.setInterval(function(){
						checkAddress();
					}, 30000);
					//console.log(tx);
					//console.log(contract);
				});
			}catch(e){
				$("#overlay").hide();
				if(timerID) Meteor.clearInterval(timerID);
			}
		})
		
	} else {
		//
	}
}
async function betDown(zilamount){
	const isConnect = await window.zilPay.wallet.connect();
	if (isConnect) {
		$("#overlay").show();
		let betting = Betting.findOne();
		
		Meteor.subscribe("contractById", betting.contract, async function(){
			let gcontract = Contract.findOne();
			const { contracts, utils, wallet } = window.zilPay;
			const contract = contracts.at(gcontract.address);
			const amount = utils.units.toQa(zilamount, utils.units.Units.Zil);
			const gasPrice = utils.units.toQa('1000', utils.units.Units.Li);
			try{
				const tx = await contract.call(
					'Down',[],
					{
						amount,
						gasPrice,
						gasLimit: utils.Long.fromNumber(2000)
					},
					true
				).then((tx) => {
					txID = tx.TranID;
					Meteor.call("updateSubmitting", betting._id, localStorage.getItem("base16"), zilamount, txID, "down");
					timerID = Meteor.setInterval(function(){
						checkAddress();
					}, 30000);
				});
			}catch(e){
				$("#overlay").hide();
				if(timerID) Meteor.clearInterval(timerID);
			}
		})
		
	} else {
		
	}
}

//Get power balance for any address
async function powerBalance(){
	const balance = await zilliqa.blockchain.getBalance(localStorage.getItem("base16"));
	//console.log(balance);
	let result = 0;
	if(balance && balance.result && balance.result.balance)
	{
			result = units.fromQa(new BN(balance.result.balance), units.Units.Zil);
	}
	Session.set("wallet_balance", result);
	$("#wallet_balance").text(result);
}
async function checkAddress(){
	let betting = Betting.findOne();
	Meteor.subscribe("contractById", betting.contract, async function(){
		let contract = Contract.findOne();
		if(contract){
			const smartContracts = await zilliqa.blockchain.getSmartContractState(
				CP.fromBech32Address(contract.address).toLowerCase()
			);
			let upnum, downnum =0;
			let exist = false;
			if(smartContracts && smartContracts.result && smartContracts.result.downbackers)
			{
			 	const holders = smartContracts.result.downbackers;
			 	Object.keys(holders).forEach(function(key, index) {
			 		if(key === localStorage.getItem("base16").toLocaleLowerCase()){
						exist = true;
			 		}
				}, holders);
				downnum = Object.keys(holders).length;
			}
			if(smartContracts && smartContracts.result && smartContracts.result.upbackers)
			{
			 	const holders = smartContracts.result.upbackers;
			 	Object.keys(holders).forEach(function(key, index) {
			 		if(key === localStorage.getItem("base16").toLocaleLowerCase()){
						exist = true;
			 		}
				}, holders);
				upnum = Object.keys(holders).length;
			}
			Meteor.call("updateUpDown", betting._id, upnum, downnum);
			if(exist === true){
				Meteor.call("updateSubmitted", betting._id, localStorage.getItem("base16"));
				$("#overlay").hide();
				Meteor.clearInterval(timerID);
			}
		}
	});
	
	
}