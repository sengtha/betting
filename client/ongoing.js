import { Template } from 'meteor/templating';
import { BettingAddress } from '../imports/collections/betting_address.js';
import { Betting } from '../imports/collections/betting.js';
import { Contract } from '../imports/collections/contract.js';
import { Asset } from '../imports/collections/asset.js';

const { Zilliqa } = require('@zilliqa-js/zilliqa');
const { BN, units } = require('@zilliqa-js/util');
const CP = require ('@zilliqa-js/crypto');
const zilliqa = new Zilliqa(Meteor.settings.public.zilliqa.testnet.host);

var timerID = null;
var txID = "";

Template.ongoing.helpers({
    zilpay: function (event){
		if (Session.get("wallet") !== 'undefined') 
		{
			return Session.get("wallet");
		}
		return false;
	},
	asset: function(id){
		Meteor.subscribe("assetById", id);
		let asset = Asset.findOne({_id:id});
		if(asset) return asset.name;
	},
	bettingadd: function(event){
			Meteor.subscribe("ongoingBetting", Session.get("wallet16"));
		return BettingAddress.find();
	},
	betting: function(id){
			Meteor.subscribe("bettingById", id);
			return Betting.findOne({_id:id});
	}
        
})

Template.ongoing.onRendered(function (event) {
        updateBlock();
	Meteor.setInterval(function(){
		updateBlock();
	}, 22500);
	Meteor.setTimeout(function(){
		
		if (typeof window.zilPay !== 'undefined') 
		{
			
			powerBalance();
			Meteor.setInterval(function(){
				powerBalance();
			}, 22500);
			//console.log(window.zilPay.wallet);
			window.zilPay.wallet.observableAccount().subscribe(function (account) {
				checkZilpayAccount(account);
			});
			window.zilPay.wallet.observableNetwork().subscribe(function (net) {
				checkZilPayNetwork(net);
			});
		}
		//console.log(window.zilPay.wallet.defaultAccount);
	}, 1200);
});

Template.ongoing.events({
        'click .wondata': function (event) {
                var currentTarget = event.currentTarget.dataset;
                if(currentTarget && currentTarget.id){
                        var ids = currentTarget.id.split(",");
                        if(ids.length>0){
                                let bettingaddrid = ids[0];
                                Meteor.call("getBettingAddressById", bettingaddrid, function(err, rs){
                                        if(rs){
                                                if(rs.up) claimUp(rs.bettingid, rs.contractid);
                                                if(rs.down) claimDown(rs.bettingid, rs.contractid);
                                        }
                                });
                        }
                }
        }                           
    
});

async function claimUp(bettingid, contractid){
	const isConnect = await window.zilPay.wallet.connect();
	
	if (isConnect) {
		$("#overlay").show();
		
			Meteor.subscribe("contractById", contractid, async function(){
				let gcontract = Contract.findOne();
				const { contracts, utils, wallet } = window.zilPay;
				const contract = contracts.at(gcontract.address);
				const amount = utils.units.toQa(0, utils.units.Units.Zil);
				const gasPrice = utils.units.toQa('1000', utils.units.Units.Li);
				try{
					const tx = await contract.call(
						'ClaimUpReward',[],
						{
							amount,
							gasPrice,
							gasLimit: utils.Long.fromNumber(2000)
						},
						true
					).then((tx) => {
						
						txID = tx.TranID;
						Meteor.call("updateClaiming", bettingid, localStorage.getItem("base16"), txID);
						timerID = Meteor.setInterval(function(){
							checkTransaction();
						}, 30000);
					});
				}catch(e){
					$("#overlay").hide();
					if(timerID) Meteor.clearInterval(timerID);
				}
			})
		
	} else {
		//
	}
}
async function claimDown(bettingid, contractid){
	const isConnect = await window.zilPay.wallet.connect();
	
	if (isConnect) {
		$("#overlay").show();
		
		Meteor.subscribe("contractById", contractid, async function(){
			let gcontract = Contract.findOne();
			const { contracts, utils, wallet } = window.zilPay;
			const contract = contracts.at(gcontract.address);
			const amount = utils.units.toQa(0, utils.units.Units.Zil);
			const gasPrice = utils.units.toQa('1000', utils.units.Units.Li);
			try{
				const tx = await contract.call(
					'ClaimDownReward',[],
					{
						amount,
						gasPrice,
						gasLimit: utils.Long.fromNumber(2000)
					},
					true
				).then((tx) => {
					//console.log(tx);
					txID = tx.TranID;
					Meteor.call("updateClaiming", bettingid, localStorage.getItem("base16"), txID);
					timerID = Meteor.setInterval(function(){
						checkTransaction();
					}, 30000);
				});
			}catch(e){
				$("#overlay").hide();
				if(timerID) Meteor.clearInterval(timerID);
			}
		})
		
	} else {
		//
	}
}

function checkZilpayAccount(account){
	console.log("Account changed:");
	if(account && account.bech32 !==""){
		Session.set("wallet", account.bech32);
		Session.set("wallet16", account.base16);
		localStorage.setItem("bech32", account.bech32);
		localStorage.setItem("base16", account.base16);
		$("#wallet_balance").text("");
	}
}
function checkZilPayNetwork(net){
	console.log("Network changed:");
	console.log(net);
}

async function updateBlock(){
	
		const numTxBlock = await zilliqa.blockchain.getNumTxBlocks();
		$("#current_block").html(numTxBlock.result);
		$(".currentblock").attr("value",numTxBlock.result);
}

//Get power balance for any address
async function powerBalance(){
	const balance = await zilliqa.blockchain.getBalance(localStorage.getItem("base16"));
	//console.log(balance);
	let result = 0;
	if(balance && balance.result && balance.result.balance)
	{
			result = units.fromQa(new BN(balance.result.balance), units.Units.Zil);
	}
	Session.set("wallet_balance", result);
	$("#wallet_balance").text(result);
}


async function checkTransaction(){
	
	const callTx = await zilliqa.blockchain.getTransaction(txID);
	const { utils } = window.zilPay;
	if(callTx && callTx.receipt && callTx.receipt.event_logs && callTx.receipt.event_logs.length>0){
		let rstext = callTx.receipt.event_logs[0]._eventname;
		if(rstext === "ClaimedReward") {
			let reward = utils.units.fromQa(new BN(callTx.receipt.event_logs[0].params[1].value), utils.units.Units.Zil);
			Meteor.call("updateClaimed", txID, reward);
	 		$("#overlay").hide();
			Meteor.clearInterval(timerID);
		}
	}
}