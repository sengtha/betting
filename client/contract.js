import { Template } from 'meteor/templating';
import { TabularTables } from '../imports/common/tabular.js';
import { Confirmation } from 'meteor/matdutour:popup-confirm';

Template.contract.helpers({
    Contract: function (event){
        return TabularTables.Contract;
    }
    
})

Template.contract.onRendered(function (event) {
});

Template.contract.events({
    'click #btnSave': function(event){

        if($("#inputname").val() !=="" && $("#inputaddress").val() !=="")
        {
                if(Session.get("contractid") !== undefined && Session.get("contractid") !=="")
                {
                        Meteor.call("updateContract", Session.get("contractid"), $("#inputname").val(), $("#inputaddress").val(), function(err, rs){
                                if(rs) {
                                        $("#inputname").val("");
                                        $("#inputaddress").val("");
                                        Session.set("contractid", undefined);
                                }
                        });
                }else{
                        Meteor.call("addContract", $("#inputname").val(), $("#inputaddress").val(), function(err, rs){
                                if(rs) {
                                        $("#inputname").val("");
                                        $("#inputaddress").val(""); 
                                }
                        });
                }
                
        }
        
    },
    'click tbody > tr': function (event) {
            //event.preventDefault();
            var dataTable = $(event.target).closest('table').DataTable();
            var rowData = dataTable.row(event.currentTarget).data();
            if(event.target.id == "edit_contract"){

                    Session.set("contractid", rowData._id);
                    Meteor.call("getContract", rowData._id, function(err, rs){
                            if(rs){
                                $("#inputname").val(rs.name), $("#inputaddress").val(rs.address);
                            }
                    });
            }
            if(event.target.id == "delete_contract"){
                    new Confirmation({
                            message: "Do you want to delete this contract",
                            title: "Confirmation",
                            cancelText: "No",
                            okText: "Yes",
                            success: true, // whether the button should be green or red
                            focus: "cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
                    }, function (ok) {
                            if(ok) Meteor.call('deleteContract', rowData._id, function(err, rs){
                                    if(!rs){
                                            alert("Cannot delete because this contract has been used.");
                                    }
                            });
                    });
            }

    }
});
