import { Template } from 'meteor/templating';
import { TabularTables } from '../imports/common/tabular.js';
import { Confirmation } from 'meteor/matdutour:popup-confirm';

Template.asset.helpers({
    Asset: function (event){
        return TabularTables.Asset;
    }
    
})

Template.asset.onRendered(function (event) {
});

Template.asset.events({
    'click #btnAdd': function(event){

        if($("#inputname").val() !=="" && $("#inputsymbol").val() !=="")
        {
                if(Session.get("assetid") !== undefined && Session.get("assetid") !=="")
                {
                        Meteor.call("updateAsset", Session.get("assetid"), $("#inputname").val(), $("#inputsymbol").val(), function(err, rs){
                                if(rs) {
                                        $("#inputname").val("");
                                        $("#inputsymbol").val("");
                                        Session.set("assetid", undefined);
                                }
                        });
                }else{
                        Meteor.call("addAsset", $("#inputname").val(), $("#inputsymbol").val(), function(err, rs){
                                if(rs) {
                                        $("#inputname").val("");
                                        $("#inputsymbol").val(""); 
                                }
                        });
                }
                
        }
        
    },
    'click tbody > tr': function (event) {
            //event.preventDefault();
            var dataTable = $(event.target).closest('table').DataTable();
            var rowData = dataTable.row(event.currentTarget).data();
            if(event.target.id == "edit_asset"){

                    Session.set("assetid", rowData._id);
                    Meteor.call("getAsset", rowData._id, function(err, rs){
                            if(rs){
                                $("#inputname").val(rs.name), $("#inputsymbol").val(rs.symbol);
                            }
                    });
            }
            if(event.target.id == "delete_asset"){
                    new Confirmation({
                            message: "Do you want to delete this asset",
                            title: "Confirmation",
                            cancelText: "No",
                            okText: "Yes",
                            success: true, // whether the button should be green or red
                            focus: "cancel" // which button to autofocus, "cancel" (default) or "ok", or "none"
                    }, function (ok) {
                            if(ok) Meteor.call('deleteAsset', rowData._id, function(err, rs){
                                    if(!rs){
                                            alert("Cannot delete because this asset has been used.");
                                    }
                            });
                    });
            }

    }
});
