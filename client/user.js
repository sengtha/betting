import { AccountsTemplates } from 'meteor/useraccounts:core';
//Setting account
AccountsTemplates.configure({
        confirmPassword: true,
        enablePasswordChange: false,
        forbidClientAccountCreation: false,
        //overrideLoginErrors: true,
        enforceEmailVerification: false,
        sendVerificationEmail: false,
        showAddRemoveServices: false,
        showForgotPasswordLink: false,
        showLabels: true,
        showPlaceholders: true,
        showResendVerificationEmailLink: false
});

AccountsTemplates.configure({
        defaultLayout: 'account',
});
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');